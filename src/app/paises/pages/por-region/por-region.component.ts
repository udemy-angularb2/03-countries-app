import { Component, OnInit } from '@angular/core';
import { PaisService } from '../../services/pais.service';
import { Countries } from '../../interfaces/paises.interface';

@Component({
  selector: 'app-por-region',
  templateUrl: './por-region.component.html',
  styles: [`
    button {
      margin-right: 5px;
    }`
  ]
})
export class PorRegionComponent {

  regiones: string[] = ['EU', 'EFTA', 'CARICOM', 'PA', 'AU', 'USAN', 'EEU', 'AL', 'ASEAN', 'CAIS', 'CEFTA', 'NAFTA', 'SAARC'];
  regionActiva = '';
  paises: Countries[] = [];

  constructor( private paisService: PaisService ) { }

  getClassCss( region: string ): string {
    return (region === this.regionActiva) ? 'btn btn-primary': 'btn btn-outline-primary';
  }

  activarRegion(region: string) {
    if( this.regionActiva === region ) { return; }
    this.regionActiva = region;
    this.paises = [];
    this.paisService.getCountriesByRegion( region )
          .subscribe( countries => this.paises = countries );
  }


}
