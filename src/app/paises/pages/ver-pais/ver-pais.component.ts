import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap, tap } from 'rxjs/operators';

import { Country } from '../../interfaces/pais.interface';
import { PaisService } from '../../services/pais.service';

@Component({
  selector: 'app-ver-pais',
  templateUrl: './ver-pais.component.html',
  styles: [
  ]
})
export class VerPaisComponent implements OnInit {

  pais !: Country;

  constructor( 
    private activeRoute: ActivatedRoute,
    private paisService: PaisService
  ) { }

  ngOnInit(): void {

    this.activeRoute.params.pipe(
      switchMap( ( {id} ) => this.paisService.getCountryByAlpha(id) ),
      tap( console.log )
    )
    .subscribe( resp => {
      this.pais = resp;
    });
    /*this.activeRoute.params.subscribe( ({id}) => {
      console.log(id);
      this.paisService.getCountryByAlpha(id).subscribe( pais => {
        console.log(pais);
      })
    });*/

  }

}
