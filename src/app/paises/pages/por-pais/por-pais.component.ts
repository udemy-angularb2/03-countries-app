import { Component, OnInit } from '@angular/core';
import { Countries } from '../../interfaces/paises.interface';
import { PaisService } from '../../services/pais.service';

@Component({
  selector: 'app-por-pais',
  templateUrl: './por-pais.component.html',
  styles: [`
    li {
      cursor: pointer;
    }
  `]
})
export class PorPaisComponent {

  termino: string = '';

  isError: boolean = false;

  countries: Countries[] = [];

  paisesSugeridos: Countries[] = [];

  mostrarSugerencias: boolean = false; 

  constructor( private paisService: PaisService ) { }

  buscar( termino: string ) {
    this.mostrarSugerencias = false;
    this.isError = false;
    this.termino = termino;
    console.log(this.termino);
    this.paisService.buscarPais(this.termino).subscribe(
      (paises) => {
        this.countries = paises;
      }, (error) => {
        this.isError = true;
        this.countries = [];
      }
    );
  }

  sugerencias(termino: string) {
    this.isError = false;
    this.mostrarSugerencias = true;
    this.termino = termino;
    this.paisService.buscarPais(termino)
      .subscribe( paises => this.paisesSugeridos = paises.splice(0,5),
        (err) => this.paisesSugeridos = [] );
  }

  buscarSugerido(termino: string) {
    this.buscar(termino);
  }


}
