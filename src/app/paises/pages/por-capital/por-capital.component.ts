import { Component, OnInit } from '@angular/core';

import { Countries } from '../../interfaces/paises.interface';
import { PaisService } from '../../services/pais.service';

@Component({
  selector: 'app-por-capital',
  templateUrl: './por-capital.component.html',
  styles: [
  ]
})
export class PorCapitalComponent {

  termino: string = '';

  isError: boolean = false;

  countries: Countries[] = [];

  constructor( private paisService: PaisService ) { }

  buscar( termino: string ) {
    this.isError = false;
    this.termino = termino;
    this.paisService.buscarCapital(this.termino).subscribe(
      (paises) => {
        this.countries = paises;
      }, (error) => {
        this.isError = true;
        this.countries = [];
      }
    );
  }

}
