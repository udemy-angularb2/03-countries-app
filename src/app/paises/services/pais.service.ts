import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { Country } from '../interfaces/pais.interface';
import { Countries } from '../interfaces/paises.interface';

@Injectable({
  providedIn: 'root'
})
export class PaisService {

  private apiURL: string = 'https://restcountries.com/v2';

  constructor( private httpClient: HttpClient) { }

  buscarPais(termino: string): Observable<Countries[]> {
    const url = `${this.apiURL}/name/${termino}`;
    return this.httpClient.get<Countries[]>( url, {params: this.httpParams } );
    /*return this.httpClient.get( url ).pipe(
      catchError( err => of([]) )
    );*/
  }

  buscarCapital(termino: string): Observable<Countries[]> {
    const url = `${this.apiURL}/capital/${termino}`;
    return this.httpClient.get<Countries[]>( url, { params: this.httpParams } );
  }

  getCountryByAlpha(termino: string): Observable<Country> {
    const url = `${this.apiURL}/alpha/${termino}`;
    return this.httpClient.get<Country>( url );
  }

  getCountriesByRegion(region: string): Observable<Countries[]> {

    const url = `${this.apiURL}/regionalbloc/${region}`;

    return this.httpClient.get<Countries[]>( url, { params: this.httpParams }).pipe(
      tap(console.log)
    );
  }

  get httpParams(): HttpParams {
    return new HttpParams()
      .set('fields', 'name,capital,alpha2Code,flags');
  }

}
